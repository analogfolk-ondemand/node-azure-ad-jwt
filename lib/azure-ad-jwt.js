const AzureActiveDirectoryValidationManager = require('./azure-ad-validation-manager.js');

module.exports.verify = async function (jwtString, options = {}) {
    return new Promise(async (resolve, reject) => {
        try {
            const aadManager = new AzureActiveDirectoryValidationManager();
            options.isB2C = false;

            // get the tenant id from the token
            const tenantId = aadManager.getTenantId(jwtString);
            let userFlow;

            // check if it looks like a valid AAD token
            if (!tenantId) {
                throw new Error(-1, 'Not a valid AAD token');
            }

            const b2cData = aadManager.getB2CData(jwtString);
            userFlow = b2cData.userFlow;
            if (userFlow) {
                // if the token doesn't have a tenantId but does have userFlow
                // let's assume we have a token issued by the B2C flow
                options.isB2C = true;
                options.kid = b2cData.kid;
            }

            // download the open id config
            const openIdConfig = await aadManager.requestOpenIdConfig(tenantId, userFlow, options);

            // download the signing certificates from Microsoft for this specific tenant
            const certificates = await aadManager.requestSigningCertificates(openIdConfig.jwks_uri, options);
            // verify against all certificates
            const verification = aadManager.verify(jwtString, certificates, openIdConfig, options);
            resolve(verification);
        } catch (err) {
            console.log('azure-ad-jwt ERROR', err);
            reject(err);
        }
    });
}